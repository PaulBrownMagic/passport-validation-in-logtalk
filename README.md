# Passport validation

Run with various backends:

```bash
:~$ swilgt -s loader.lgt
```

```bash
:~$ lvmlgt -f load.pl
```

```bash
:~$ eclipselgt -f loader.lgt
```

etc...

Then check the loading output for the counts (I saved you typing the query).
