:- object(validator).

	:- info([
		version is 1:0:0,
		author is 'Paul Brown',
		date is 2021-03-12,
		comment is 'Checks a given file for valid passports'
	]).

	:- uses(meta, [include/3, map/3]).
	:- uses(list, [length/2]).

	:- public(file_valid_passports/3).
	:- mode(file_valid_passports(+atom, +list, -list), zero_or_one).
	:- info(file_valid_passports/3, [
		comment is 'Find the valid passports in a given file',
		argnames is ['File', 'RequiredFields', 'Passports']
	]).
	file_valid_passports(File, RequiredFields, ValidPassports) :-
		passport_reader::file_passports(File, Passports),
		include(valid_passport(RequiredFields), Passports, ValidPassports).

	valid_passport(RequiredFields, Passport) :-
		map(passport(Passport)::field, RequiredFields, _Values).

	:- public(file_valid_passport_count/3).
	:- mode(file_valid_passport_count(+atom, +list, -int), zero_or_one).
	:- info(file_valid_passport_count/3, [
		comment is 'How many passports are valid',
		argnames is ['File', 'RequiredFields', 'Count']
	]).
	file_valid_passport_count(File, RequiredFields, Count) :-
		file_valid_passports(File, RequiredFields, Passports),
		length(Passports, Count).

:- end_object.
