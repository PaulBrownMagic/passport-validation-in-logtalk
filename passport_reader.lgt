:- object(passport_reader).

	:- info([
		version is 1:0:0,
		author is 'Paul Brown',
		date is 2021-03-12,
		comment is 'Reads in passport data from file'
	]).

    :- uses(reader, [file_to_chars/2]).

	:- public(file_passports/2).
	:- mode(file_passports(+atom, -list), zero_or_one).
	:- info(file_passports/2, [
		comment is 'Given a file, read the passport data from it',
		argnames is ['File', 'Passports']
	]).
	file_passports(File, Passports) :-
		file_to_chars(File, Chars),
		chars_passport(Chars, Passports).


	:- public(chars_passports/2).
	:- mode(chars_passports(+list(chars), -list), zero_or_one).
	:- info(chars_passports/2, [
		comment is 'Given a list of characters, read the passport data from it',
		argnames is ['Chars', 'Passports']
	]).
	chars_passport(Chars, Passports) :-
		once(phrase(passports(Passports), Chars)).

	% A list of passports
	passports([Passport|Passports]) -->
		passport(Passport),
		newline,
		newline,
		passports(Passports).
	passports([Passport|[]]) -->
		passport(Passport).  % end of file has no new line, ends abruptly

	% A passport is a list of fields, seperated by blank chars
	passport([Field|Fields]) -->
		field(Field),
		blank,
		passport(Fields).
	passport([Field|[]]) -->
		field(Field).

	% A field is a key-value pair
	field(Key-Value) -->
		key(Key),
		sep,
		value(Value).

	% Keys are known
	key(byr) --> "byr".
	key(iyr) --> "iyr".
	key(eyr) --> "eyr".
	key(hgt) --> "hgt".
	key(hcl) --> "hcl".
	key(ecl) --> "ecl".
	key(pid) --> "pid".
	key(cid) --> "cid".

	sep --> ":".

	% Values are whatever follows the seperator, but no blanks
	value('') --> "".
	value(Value) -->
		[C],
		{ \+ phrase(blank, [C]) },
		value(Cs),
		{ atom_concat(C, Cs, Value) }.

	blank --> " ".
	blank --> newline.
	newline --> "\n".

:- end_object.
