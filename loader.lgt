:- initialization((
	% Using DCGs, let's make them easy to read:
	set_prolog_flag(double_quotes, chars),
	% Stdlib imports
	logtalk_load([
		meta(loader),
		reader(loader),
		types(loader)
	]),
	% Local imports
	logtalk_load([
		passport_reader,
		passport,
		validator
	]),
	% Show it works... it might not be correct, but it runs!
	MostFields = [byr, iyr, eyr, hgt, hcl, ecl, pid],
	validator::file_valid_passport_count('aoc4.txt', [], InvalidCount),
	validator::file_valid_passport_count('aoc4.txt', MostFields, MostCount),
	validator::file_valid_passport_count('aoc4.txt', [cid|MostFields], AllCount),
	write('Number of passports including invalid': InvalidCount), nl,
	write('Number of valid passports without "cid"': MostCount), nl,
	write('Number of valid passports with "cid"': AllCount), nl
)).
