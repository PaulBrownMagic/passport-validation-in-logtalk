:- object(passport(_Fields_)).

	:- info([
		version is 1:0:0,
		author is 'Paul Brown',
		date is 2021-03-12,
		comment is 'A parametric object for accessing passport field data'
	]).

	:- uses(list, [member/2]).

	:- public(field/2).
	:- mode(field(?atom, -atom), zero_or_more).
	:- info(field/2, [
		comment is 'A field is a named bit of data associated with the passport',
		argnames is ['FieldName', 'Value']
	]).
	field(Name, Value):-
		member(Name-Value, _Fields_).

:- end_object.
